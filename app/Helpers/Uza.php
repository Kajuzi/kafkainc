<?php

/**
* Helper functions to use everywhere
*/
class Uza
{

	function __construct()
	{
		# code...
	}

	public static function spit($value='')
	{
		return true;
	}

	public static function can($action, $table)
	{
		if(is_integer($table))
			$tafel = \App\Tafel::find($table);
		else
			$tafel = \App\Tafel::where('table_name', $table)->first();

		if($action == 'view')
			$allowed = explode(',', $tafel->viewable_by);
		else
			$allowed = explode(',', $tafel->editable_by);

        if(array_intersect($allowed, \Auth::user()->get_roles()))
            return true;
        else
            return false;
	}

	public static function owns($table, $row_id = NULL)
	{
		if(is_integer($table))
			$tafel_id = 'id';
		else
			$tafel_id = 'table_name';

		if(is_integer($row_id))
			$row = DB::table($tafel_id)->where('id', $row_id);

		if(isset($row)){
	        if($row->created_by == \Auth::user()->id)
	            return true;
	        else
	            return \Auth::user()->hasRole('admin');			
		}else{
			return false;
		}
	}
}
