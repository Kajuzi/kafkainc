<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use App\Column;

class Tafel extends Model
{
    /**
     * The table where information about other tables is stored.
     *
     * @var string
     */
    protected $table = 'tafels';

    protected $fillable = ['description', 'viewable_by', 'editable_by'];

    /**
    * Get array of all tables with their properties
    * @var array
    */
    public static function get_all()
    {
        $table_list_object = DB::select('SHOW TABLES');
        $tbl_name_variable = "Tables_in_".getenv('DB_DATABASE');
        foreach ($table_list_object as $table => $tbl_name) {
            $tables[$tbl_name->{$tbl_name_variable}]['name'] = $tbl_name->{$tbl_name_variable};
            $tables[$tbl_name->{$tbl_name_variable}]['columns'] = Schema::getColumnListing($tbl_name->{$tbl_name_variable});
            $tables[$tbl_name->{$tbl_name_variable}]['size'] = DB::table($tbl_name->{$tbl_name_variable})->count();
        }
        return $tables;
    }

    /**
    * Get an array of all table names
    * @var array
    */
    public static function list_all()
    {
        $table_list_object = DB::select('SHOW TABLES');
        $tbl_name_variable = "Tables_in_".getenv('DB_DATABASE');
        foreach ($table_list_object as $table => $tbl_name) {
            $tables[] = $tbl_name->{$tbl_name_variable};
        }
        return $tables;
    }

    /**
    * Get an array of all table names the current logged in user can access
    * @var array
    */
    public static function list_accessible()
    {
        $table_list_object = DB::select('SHOW TABLES');
        $tbl_name_variable = "Tables_in_".getenv('DB_DATABASE');
        foreach ($table_list_object as $table => $tbl_name) {
            $tables[] = $tbl_name->{$tbl_name_variable};
        }
        return $tables;
    }

    /**
    * Get list of all accessible tables
    * @var object
    */
    public static function get_accessible()
    {
        $table_list_object = DB::table('tafels')->get();
        $tables = array();
        foreach ($table_list_object as $row => $table) {
            if (Tafel::is_viewable($table->id)) {
                $table->columns = Schema::getColumnListing($table->table_name);
                $table->size = DB::table($table->table_name)->count();
                $tables[$table->table_name] = $table;
            }
        }
        return $tables;
    }

    /**
    * Get paginated records of one table
    * @var object
    */
    public static function get_records($id)
    {
        $table_name = Tafel::find($id)->table_name;
        return DB::table($table_name)->paginate(15); // TODO dynamic pagination
    }

    /**
    * Get one table
    * @var object
    */
    public static function get_one($id)
    {
        Tafel::refresh_tafels();
        $table = Tafel::find($id);
        $table->columns = \App\Column::where('tafel_id', $id)->get();
        return $table;
    }

    /**
    * Check if table is viewable by loged in user
    * @var object
    */
    public static function is_viewable($id)
    {
        $row = Tafel::find($id);
        $allowed = explode(',', $row->viewable_by);
        if(array_intersect($allowed, \Auth::user()->get_roles()))
            return true;
        else
            return false;
    }

    /**
    * Make sure all tables are registered in the `tafels` table
    * @var boolean
    */
    public static function refresh_tafels()
    {
        $table_list_object = DB::select('SHOW TABLES');
        $tbl_name_variable = "Tables_in_".getenv('DB_DATABASE');
        $inserts = [];
        foreach ($table_list_object as $table => $tbl_name) {
            $table_name = $tbl_name->{$tbl_name_variable};
            if(!DB::table('tafels')->where('table_name', '=', $table_name)->first())
            {
                // The table does not exist so we'll enter it
                if(DB::table('tafels')->insert([
                                                'table_name' => $table_name,
                                                'viewable_by' => 'member,admin',
                                                'editable_by' => 'member,admin'
                                                ]))
                    $inserts[] = $table_name;
            }
            // Make sure all columns are entered into the the `columns` table

            $columns = Schema::getColumnListing($table_name);
            $tafel = Tafel::where('table_name', $table_name)->first();

            if (!in_array($table_name, config('constants.system_tables'))) {
                /*
                *   Every table should have id, created_at, created_by, edited_at and last_edited_by
                */
                if (!in_array('id', $columns) ) {
                    \DB::statement('ALTER TABLE `'.$table_name.'` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`)');
                }
                
                if (!in_array('created_at', $columns)) {
                    \DB::statement('ALTER TABLE `'.$table_name.'` ADD `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, ADD `updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_at`');
                }
                
                if (!in_array('created_by', $columns)) {
                    \DB::statement("ALTER TABLE `".$table_name."` ADD `created_by` INT NOT NULL DEFAULT '1', ADD `last_updated_by` INT NOT NULL DEFAULT '1' AFTER `created_by`");
                }                   
            }            

            
            foreach ($columns as $col_name) {
                if(!DB::table('columns')
                        ->where('column_name', $col_name)
                        ->where('tafel_id', $tafel->id)
                        ->first())
                {
                    // The column does not exist in the so we'll enter it
                    DB::table('columns')->insert([
                                                    'tafel_id' => $tafel->id,
                                                    'column_name' => $col_name,
                                                    'field_type' => 'text',
                                                    'validation_rules' => '',
                                                    'acceptable_values' => ''
                                                    ]);
                }
            }
        }
        return $inserts;
    }

    /**
    * Make sure all tables are registered in the `tafels` table
    * @var boolean
    */
    public static function resetids($id)
    {
        $table = Tafel::find($id);

       \DB::statement('ALTER TABLE `'. $table->table_name .'` DROP `id`');
       \DB::statement('ALTER TABLE `'. $table->table_name .'` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`)');
    }

    public static function bijwerken($request, $id)
    {
        $columns = [];
        $tafel_data = ['last_modified_by' => \Auth::user()->id];
        foreach ($request->all() as $field => $value) {
            $exploded   = explode('__', $field);
            if (sizeof($exploded) > 1)
                $columns[$exploded[0]][$exploded[1]] = $value;
            else
                $tafel_data[$field] = $value;
        }
        foreach ($columns as $col_id => $col_data) {
            $col = Column::find($col_id);
            $col->fill($col_data);
            $col->save();
        }
        $tafel_data['editable_by'] = implode(',', $tafel_data['editable_by']);
        $tafel_data['viewable_by'] = implode(',', $tafel_data['viewable_by']);
        $tafel = Tafel::find($id);
        $tafel->fill($tafel_data);
        $tafel->save();
        return true;
    }

    public static function fields($table_name, $id = null)
    {
    	if ($id)
            $row = DB::table($table_name)->where('id', $id)->first();

        $tafel = DB::table('tafels')->where('table_name', $table_name)->first();
        $columns = \App\Column::where('tafel_id', $tafel->id)->get();

        if (!$id) {
            $row = [];
            foreach ($columns as $clm) {
                $row[$clm->column_name] = '';
            }
        }

        $data = $cols = [];
        foreach ($columns as $c) {
            $cols[$c->column_name] = $c;
        }

        foreach (config('constants.system_columns') as $sc) {
            unset($cols[$sc]); // System columns should not be edited by user
        }

        foreach ($row as $key => $col) {
            $data[$key]['value'] = $col;
            if(isset($cols[$key]))
                switch ($cols[$key]->field_type) {
                    case 'text':
                        $data[$key]['field'] = \Form::text($key, $col, ['class'=>'form-control']);
                        break;

                    case 'date':
                        // $data[$key]['field'] = \Form::date($key, \Carbon\Carbon::now());
                        $data[$key]['field'] = \Form::date($key, $col);
                        break;

                    case 'checkbox':
                        $data[$key]['field'] = '';
                        $exploded = explode(',', $cols[$key]->acceptable_values);
                        foreach ($exploded as $avalue) {
                            $data[$key]['field'] .= $avalue.' '.\Form::checkbox($key, $avalue, in_array($col, $exploded)).'<br>';
                        }
                        break;

                    case 'select':
                        $exploded = explode(',', $cols[$key]->acceptable_values);
                        foreach ($exploded as $avalue) {
                            $exlopd[$avalue] = $avalue;
                        }
                        $data[$key]['field'] = \Form::select($key, $exlopd, $col);
                        break;

                    case 'email':
                        $data[$key]['field'] = \Form::email($key, $col, ['class'=>'form-control']);
                        break;

                    default:
                        $data[$key]['field'] = \Form::text($key, $col, ['class'=>'form-control']);
                        break;
                }
            else {
                $data[$key]['field'] = $col;
            }
        }
        return $data;
    }
}
