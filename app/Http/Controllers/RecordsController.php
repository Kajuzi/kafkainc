<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tafel;
use App\Record;
use App\Http\Requests\RecordPostRequest;
use App\Http\Requests\RecordPatchRequest;

class RecordsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($table_name)
    {
        $page['table_name'] = $table_name;
        $page['fields'] = Tafel::fields($table_name);
        return view('record-create', $page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($table_name, RecordPostRequest $request)
    {
        if($id = Record::insert_row($table_name, $request))
            return redirect()->route('records.show', [$table_name, $id]);
        else
            return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($table_name, $id)
    {
        $page['table_name'] = $table_name;
        $page['tables'] = Tafel::get_all();
        $page['record'] = Record::get_one($table_name, $id);
        return view('record-show', $page);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($table_name, $id)
    {
        $page['table_name'] = $table_name;
        $page['tables'] = Tafel::get_all();
        $page['record'] = Record::get_one($table_name, $id);
        $page['fields'] = Tafel::fields($table_name, $id);
        return view('record-edit', $page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $table_name
     * @param  int  $id
     */
    public function update(RecordPatchRequest $request, $table_name, $id)
    {
        $post = $request->input();
        $post['id'] = $id; // The id should not come from the form
        $update = Record::update_row($table_name, $post);
        return redirect(route('records.show',[$table_name, $id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
