<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tafel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application home.
     *
     */
    public function index()
    {
        return view('home');
    }
    

    /**
     * Show the application dashboard.
     *
     */
    public static function dashboard()
    {
        $page['tables'] = Tafel::get_accessible();
        return view('dashboard', $page);
    }

}
