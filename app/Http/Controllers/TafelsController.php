<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tafel;

class TafelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['tables'] = Tafel::get_accessible();
        return view('tables', $page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return "<h2>Feature not yet available</h2>";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $table_name
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page['table'] = Tafel::find($id);
        $page['tables'] = Tafel::get_accessible();
        $page['records'] = Tafel::get_records($id);
        return view('table-show', $page);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page['table'] = Tafel::get_one($id);
        $page['roles'] = \App\Role::all();
        return view('table-edit', $page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Tafel::bijwerken($request, $id))
            return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Refresh tables to correct errors to do with IDs
     *
     * @return \Illuminate\Http\Response
     */
    public function refresh_tafels()
    {
        Tafel::refresh_tafels();
        return redirect()->back();
    }

    /**
     * Refresh tables to correct errors to do with IDs
     *
     * @return \Illuminate\Http\Response
     */
    public function reset($table_id)
    {
        Tafel::resetids($table_id);
        return redirect('tables');
    }
}
