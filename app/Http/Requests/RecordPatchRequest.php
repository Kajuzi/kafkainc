<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Tafel;

class RecordPatchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $record = \DB::table($this->table_name)->where('id', $this->id)->first();
        // Get table access rights
        $table = Tafel::where('table_name', $this->table_name)->first();
        if(in_array('member', explode(',', $table->editable_by))) {
            if($this->user()->id == $record->created_by)
                return true;
            else 
                return $this->user()->hasRole('admin');            
        } else {
            return $this->user()->hasRole('admin');
        }
        // TODO Better looking errors
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return \App\Column::validation($this->table_name, 'post');
    }
}
