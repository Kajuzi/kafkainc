<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecordStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // Get table access rights
        $table = Tafel::where('table_name', $this->table_name)->first();
        if(in_array('member', explode(',', $table->editable_by))) {
            return true;
        } else {
            return $this->user()->hasRole('admin');
        }
        // TODO Better looking errors
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return \App\Column::validation($this->table_name, 'post');
    }
}
