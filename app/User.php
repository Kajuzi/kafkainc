<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Klaravel\Ntrust\Traits\NtrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use NtrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname', 'lname', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected static $roleProfile = 'user';

    /**
     * Get IDs of all the user's roles
     *
     * @var array
     */
    public function role_ids()
    {
        $all = \DB::table('role_user')->where('user_id', '=', $this->id)->get(['role_id']);
        $roles = array();
        foreach ($all as $row) {
            $roles[] = $row->role_id;
        }
        return $roles;
    }
    
    /**
     * Get all the user's roles
     *
     * @var array
     */
    public function get_roles()
    {
        // return $this->belongsToMany('App\Role','role_user');
        $all = \DB::table('role_user')->where('user_id', '=', $this->id)->get(['role_id']);
        $roles = array();
        foreach ($all as $row) {
            $role = \DB::table('roles')->where('id', '=', $row->role_id)->first();
            $roles[] = $role->name;
        }
        return $roles;
    }   

    /**
     * Get all the user's roles
     *
     * @var array
     */
    public function get_state()
    {
        return "Active"; //TODO check if user is disabled or not
    }  

    /**
     * Get all the user's roles
     *
     * @var object
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role','role_user');
    }

}
