<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Column extends Model
{
	protected $fillable = ['field_type', 'validation_rules', 'acceptable_values'];

	/**
	 * Update a column
	 *
	 */
	public static function bijwerken($id, $input)
	{
	    $column = Column::find($id);
	    $input['last_updated_by'] = \Auth::user()->id;
	    $column->fill($input);
	    if($column->save())
	        return $column;
	    else
	        return FALSE;
	}
	
	public static function validation($table_name, $method)
	{
	    $tafel = Tafel::where('table_name', $table_name)->first();
	    $columns = Column::where('tafel_id', $tafel->id)->get();
	    $cols = [];
	    foreach ($columns as $col) {
	    	if ($col->validation_rules)
	    		$cols[$col->column_name] = $col->validation_rules;
	    }
	    return $cols;
	}
}
