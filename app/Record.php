<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Record extends Model
{
    public static function get_one($table_name, $id)
    {
    	return DB::table($table_name)->where('id', $id)->first();
    }

    public static function update_row($table_name, $data)
    {
    	$id = $data['id'];
        foreach (config('constants.system_columns') as $sc) {
            unset($data[$sc]); // id, created_at, etc should not be updatable by user
        }    	
    	unset($data['_token']);

    	return DB::table($table_name)
		            ->where('id', $id)
		            ->update($data);
    }

    public static function insert_row($table_name, $request)
    {
        $data = $request;
        unset($data['_token']);
        return DB::table($table_name)->insertGetId($request->all());
    }
}
