<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Utility extends Model
{
    
    /**
     * Update a user
     *
     * @var object
     */
    public static function bijwerken($request, $user)
    {
        $input = $request->all();
        $input['password'] = $input['password']?\Hash::make($input['password']):$user->password;
        $user->fill($input);
        $roles = array();
        $user->roles()->detach();
        foreach ($input['roles'] as $role_id) {
        	$user->roles()->attach($role_id);
        }
        if($user->save())
            return $user;
        else
            return FALSE;
    }
}
