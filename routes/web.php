<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group.
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['admin'])->group(function () {
	Route::resource('users', 'UsersController');
    Route::get('roles/{id}/users', function () {
        return redirect('/users');
    });
    
    //Route::get('tafels/refresh', 'TafelsController@something');
    Route::get('resetid/{table_id}', 'TafelsController@reset');
    Route::resource('tables', 'TafelsController', ['only' => [
        'create', 'store', 'edit', 'update', 'destroy'
    ]]);
    Route::get('tafels/refresh', 'TafelsController@refresh_tafels')->name('tafels.refresh');

    Route::get('roles', 'TafelsController@make_roles')->name('roles'); // TODO remove on production
});

Route::middleware(['auth'])->group(function () {
    Route::get('dashboard', 'HomeController@dashboard')->name('dashboard');
    Route::resource('tables', 'TafelsController', ['except' => [
        'create', 'store', 'edit', 'update', 'destroy'
    ]]);
    Route::get('records/{table_name}/create', 'RecordsController@create')->name('records.create');
    Route::post('records/{table_name}', 'RecordsController@store')->name('records.store');
	Route::get('records/{table_name}/{id}', 'RecordsController@show')->name('records.show');
	Route::get('records/{table_name}/{id}/edit', 'RecordsController@edit')->name('records.edit');
	Route::post('records/{table_name}/{id}', 'RecordsController@update')->name('records.update');
});
