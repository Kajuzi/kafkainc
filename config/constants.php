<?php

return [

    'system_tables' => ['permissions', 'roles', 'permission_role', 'roles', 'role_user', 'password_resets', 'migrations', 'tafels'],

    'system_columns' => ['id', 'created_at', 'updated_at', 'created_by', 'last_updated_by' ],
];
