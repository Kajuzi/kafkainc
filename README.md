# Generic Web Front End for MYSQL Database

## Installation
- Extract the zip into the folder you want the app to be in
- Edit .env file with your database values (assuming you've already created the database)
- In .env set the chache driver to 'array' (``CACHE_DRIVER=array``)
- Import the database dump file (database_dump.sql) into the app database

## Caution
- Make sure every table have the column 'id' and it's a primary key set to auto increment

## Column Validation
To control user input, you will need to set validation rules. Please see the [validation rules documentation file](validation.md) to learn to use them.

## Errors
### General error: 1364 Field 'id' doesn't have a default value
Fix this by going to _Manage Tables_ then click "edit" on the right of the table you were trying to write to. At the bottom of the page click "Reset id column". The table's id's will be be dropped and reallocated.

### Integrity constraint violation: 1048 Column '`column_name`' cannot be null
Either go to the database and make the column nullable by ticking the NULL checkbox or make the field required by editing the table using table manager. For the latter navigate to _Manage Tables_, click "edit" to the right of the table you were trying to right to. Go to the column giving the erro and add `required` to the validation rules. Remember validation rules are separated by the symbol `|`. 

### Long URLs
Switch on your webserver's rewrite engine. This may be _mod\_rewrite_ on Apache