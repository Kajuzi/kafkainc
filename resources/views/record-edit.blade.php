@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Editing Record</div>

                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <p>- {{ $error }}</p>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {!! Form::open(array('url' => route('records.update', [$table_name, $record->id]))) !!}
                        <table class="table table-bordered table-striped">
                            <thead>
                                <th>Column</th>
                                <th>Value</th>
                            </thead>
                                @foreach ($fields as $column => $value)
                                <tr>
                                    <td>{{ $column }}</td>
                                    <td>{!! $value['field'] !!}</td>
                                </tr>
                                @endforeach
                        </table>
                        {!! Form::submit('Save Record') !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
