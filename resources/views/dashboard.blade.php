@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <div class="col-md-3">
                        <ul class="left-nav">
                            @foreach ($tables as $tafel)
                                <li><a href="{{ url('/tables/'.$tafel->id) }}"> {{ $tafel->table_name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>Table Name</th>
                              <th>Description</th>
                              <th>Number of Records</th>
                            </tr>
                          </thead>
                          <tbody>
                                @foreach ($tables as $tafel)
                                    <tr>
                                      <th scope="row"><a href="{{ url('/tables/'.$tafel->id) }}"> {{ $tafel->table_name }}</a></th>
                                      <td>
                                        @if( $tafel->description )
                                        <b>Description:</b>&nbsp;{{ $tafel->description }}<br>
                                        @endif
                                        <b>Columns:</b><br>
                                        @foreach ($tafel->columns as $column)
                                            {{ $column }},&nbsp;
                                        @endforeach
                                      </td>
                                      <td>{{ $tafel->size }}</td>
                                    </tr>
                                @endforeach

                          </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
