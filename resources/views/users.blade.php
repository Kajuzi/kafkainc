@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <div class="col-md-3">
                        <ul class="left-nav">
                            @foreach ($roles as $role)
                                <li><a href="{{ url('/roles/'.$role['id'].'/users') }}"> {{ str_plural($role['display_name']) }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>Name</th>
                              <th>Roles</th>
                              <th>Email</th>
                            </tr>
                          </thead>
                          <tbody>                                
                                @foreach ($users as $user)
                                    <tr>
                                      <th scope="row"><a href="{{ url('/users/'.$user['id']) }}"> {{ $user['fname'] }} {{ $user['lname'] }}</a></th>
                                      <td>
                                        @foreach ($user->roles as $role)
                                            {{ $role->name }},&nbsp;
                                        @endforeach
                                      </td>
                                      <td>{{ $user['email'] }}</td>
                                    </tr>                                    
                                @endforeach

                          </tbody>
                        </table>                       
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
