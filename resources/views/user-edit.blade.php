@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $user->fname }} {{ $user->lname }}</div>

                <div class="panel-body">
                    {{ Form::open([ 'method' => 'PATCH', 'route' => ['users.update', $user->id] ]) }}
                        <table class="table table-bordered table-striped">
                                <tr>
                                    <td>First Name</td>
                                    <td>{{ Form::text('fname', $user->fname, ['class' => 'form-control']) }}</td>
                                </tr>
                                <tr>
                                    <td>Last Name</td>
                                    <td>{{ Form::text('lname', $user->lname, ['class' => 'form-control']) }}</td>
                                </tr>
                                <tr>
                                    <td>Username</td>
                                    <td> {{ Form::text('username', $user->username, ['class' => 'form-control']) }} </td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td> {{ Form::email('email', $user->email, ['class' => 'form-control']) }} </td>
                                </tr>
                                <tr>
                                    <td>Roles</td>
                                    <td>
                                        @foreach($roles as $role)
                                            {{ $role->display_name }}&nbsp;{{ Form::checkbox('roles[]', $role->id, $user->hasRole($role->name)) }}<br>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td>Set Password</td>
                                    <td> {{ Form::password('password', $value = null, $attributes = ['class' => 'form-control']) }} </td>
                                </tr>
                                <tr>
                                    <td>State</td>
                                    <td>{{ $user->state }}</td>
                                </tr>
                        </table>
                        {{ Form::submit('Save', ['class'=>'btn btn-info']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
