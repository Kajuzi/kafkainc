@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $user->fname }} {{ $user->lname }}</div>

                <div class="panel-body">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td>Name</td>
                            <td>{{ $user->fname }} {{ $user->lname }}</td>
                        </tr>
                        <tr>
                            <td>Username</td>
                            <td> {{ $user->username }} </td>
                        </tr>
                        <tr>
                            <td>Roles</td>
                            <td>
                                @foreach ($user->roles as $role)
                                    {{ $role->name }}&nbsp;
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>Registered On</td>
                            <td> {{ $user->created_at }} </td>
                        </tr>
                        <tr>
                            <td>Active</td>
                            <td>{{ $user->state }}</td>
                        </tr>
                    </table>
                    @if( \Auth::user()->hasRole('admin') || \Auth::user()->id == $user->id )
                        <a class="btn btn-info" href="{{ route('users.edit', [$user->id]) }}">Edit Profile</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
