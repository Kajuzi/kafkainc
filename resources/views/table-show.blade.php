@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>{{ $table->table_name }}</h2></div>

                <div class="panel-body">
                    <div class="col-md-12">
                        Other Available Tables: <br>
                        <span class="">
                            @foreach ($tables as $tafel)
                                <a href="{{ url('/tables/'.$tafel->id) }}" class="btn btn-info" role="button"> {{ $tafel->table_name }}</a>
                            @endforeach  
                            <p>&nbsp;</p>
                            @if(\Auth::user()->hasRole('admin') && !in_array($tafel->table_name, config('constants.system_tables')))
                                <a href="{{ route('tables.edit', $table->id) }}" class="btn btn-danger" role="button">Edit This Table</a>
                            @endif  
                            @if(Uza::can('edit', $table->table_name))
                                <a href="{{ url('/records/'.$table->table_name.'/create') }}" class="btn btn-info" role="button"> New Record</a>
                            @endif                     
                        </span>
                    </div>
                    <div class="col-md-12">
                        {{ $records->links() }}  
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                              <thead>
                                <tr>
                                    @foreach ($tables[$table->table_name]->columns as $column_name)
                                        <th>{{ $column_name }}</th>
                                    @endforeach
                                </tr>
                              </thead>
                              <tbody>
                                    @foreach ($records as $row)
                                        <tr>
                                            @foreach ($row as $column)
                                                <td>{{ $column }}</td>
                                            @endforeach
                                          
                                            @if(isset($row->id))
                                                <td>
                                                    <a href="{{ url('records/'.$table->table_name, $row->id) }}">Details</a>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                              </tbody>
                            </table>                            
                        </div>
                        {{ $records->links() }}                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
