@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Editing table <b><em>{{ $table->table_name }}</em></b></h2></div>

                <div class="panel-body">
                    {{ Form::open([ 'method' => 'PATCH', 'route' => ['tables.update', $table->id] ]) }}
                        <table class="table table-bordered table-striped">
                                <tr>
                                    <td>Viewable By</td>
                                    <td>
                                        @foreach($roles as $role)
                                            {{ str_plural($role->display_name) }}&nbsp;&nbsp;{{ Form::checkbox('viewable_by[]', $role->name, in_array($role->name, explode(',', $table->viewable_by))) }}<br>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td>Editable By</td>
                                    <td>
                                        @foreach($roles as $role)
                                            {{ str_plural($role->display_name) }}&nbsp;&nbsp;{{ Form::checkbox('editable_by[]', $role->name,  in_array($role->name, explode(',', $table->editable_by))) }}<br>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td>Table Description</td>
                                    <td>{{ Form::textarea('description', $table->description, ['class' => 'form-control', 'rows'=>3]) }}</td>
                                </tr>
                        </table>
                        <h2>Columns</h2>
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>Column Name</th>
                                <th>Type</th>
                                <th>Validation Rules</th>
                                <th>Acceptable Values</th>
                            </tr>
                            @foreach($table->columns as $col)
                                <tr>
                                    <td>{{ $col->column_name }}</td>
                                    <td>
                                        {{ Form::select($col->id.'__field_type', ['text'=>'Text', 'date'=>'Date', 'checkbox'=>'Check Boxes', 'radio'=>'Radio Buttons', 'select'=>'Drop Down'], $col->field_type, ['class'=>'form-control']) }}
                                    </td>
                                    <td>{{ Form::text($col->id.'__validation_rules', $col->validation_rules, ['class' => 'form-control']) }}</td>
                                    <td>{{ Form::text($col->id.'__acceptable_values', $col->acceptable_values, ['class' => 'form-control']) }}</td>
                                </tr>
                            @endforeach
                        </table>
                        {{ Form::submit('Save', ['class'=>'btn btn-info']) }}
                        <a href="{{ url('resetid/'.$table->id ) }}" role="button" class="btn btn-danger">Reset IDs</a>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
