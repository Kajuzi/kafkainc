@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Showing record <strong>{{ $record->id }}</strong> on the <strong><em>{{ $table_name }}</em></strong> table</div>

                <div class="panel-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <th>Column</th>
                            <th>Value</th>
                        </thead>
                        @foreach ($record as $column => $value)
                        <tr>
                            <td>{{ $column }}</td>
                            <td>{{ $value }}</td>
                        </tr>
                        @endforeach
                    </table>
                    @if(Uza::owns($table_name, $record))
                        <a class="btn btn-info" href="{{ route('records.edit', [$table_name, $record->id]) }}">Edit This Record</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
