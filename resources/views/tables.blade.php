@extends('layouts.app')

@section('content')
<?php
  if(\Auth::user()->hasRole('admin')) // Check once to avoid running the method over and over in the loops
    $is_admin = true;
  else 
    $is_admin = false;
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <div class="col-md-3">
                        <ul class="left-nav">
                            @foreach ($tables as $tafel)
                                <li><a href="{{ url('/tables/'.$tafel->id) }}"> {{ $tafel->table_name }}</a></li>
                            @endforeach
                            <br>
                            @if($is_admin)
                                <a href="{{ url('tafels/refresh') }}" class="btn btn-info" role="button">Refresh Tables</a>
                            @endif
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>Table Name</th>
                              <th>Columns</th>
                              <th>Number of Records</th>
                              @if($is_admin)
                              <th>Actions</th>
                              @endif
                            </tr>
                          </thead>
                          <tbody>
                                @foreach ($tables as $tafel)
                                    <tr>
                                      <th scope="row"><a href="{{ url('/tables/'.$tafel->id) }}"> {{ $tafel->table_name }}</a></th>
                                      <td>
                                        @foreach ($tafel->columns as $column)
                                            {{ $column }},&nbsp;
                                        @endforeach
                                      </td>
                                      <td>{{ $tafel->size }}</td>
                                      @if($is_admin)
                                      <td>
                                          <a href="{{ url('/tables/'.$tafel->id.'/edit') }}"> Edit</a>
                                      </td>
                                      @endif
                                    </tr>
                                @endforeach

                          </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
